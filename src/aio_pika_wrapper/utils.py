import os


def get_user_dir_path() -> str:
    """
    Acquire HOME-like path for storing project logs and configuration.
    """
    return (
        f'{os.path.expanduser("~")}/'
        f'.{os.environ.get("PROJECT_NAME", "aio-pika-wrapper")}'
    )


def get_config_folder_path() -> str:
    """
    Acquire HOME-like path for storing project configuration.
    """
    return os.environ.get(
        "CONFIG_FOLDER_PATH",
        f"{get_user_dir_path()}/config",
    )


def get_log_folder_path() -> str:
    """
    Acquire HOME-like path for storing project logs.
    """
    return os.environ.get(
        "LOG_FOLDER_PATH",
        f"{get_user_dir_path()}/logs",
    )
