import asyncio
import datetime as dt
import enum
import json
import logging
import os
from collections.abc import Iterable
from typing import Any, Callable, MutableMapping, Optional, Tuple, Union

import aio_pika
from aio_pika import ExchangeType
from aio_pika.abc import AbstractIncomingMessage, ConsumerTag, DeliveryMode
from aio_pika.connection import Connection
from aio_pika.exchange import Exchange
from aio_pika.pool import Pool, PoolItemContextManager, T
from aio_pika.queue import Queue

# As of 1/23, this includes book, bytes, bytearray, decimal.Decimal,
# List[FieldValue], Dict[str, 'FieldValue], float, int, None, str, dt.datetime
from pamqp.commands import Basic
from pamqp.common import FieldValue

LOGGER = logging.getLogger(__name__)


async def get_aio_async_connection() -> Connection:
    """
    Acquire asynchronous connection using aio-pika.
    """
    host = os.environ.get("RABBITMQ_HOST")
    login = os.environ.get("RABBITMQ_USER")

    LOGGER.debug("CONNECTING USING %s, %s", host, login)
    return await aio_pika.connect(
        host=host,
        login=login,
        password=os.environ.get("RABBITMQ_PASS"),
    )


class AsyncState(enum.Enum):
    BINDING_QUEUE = enum.auto()
    CONNECTING = enum.auto()
    CONNECTED = enum.auto()
    CONSUMING = enum.auto()
    DECLARING_EXCHANGE = enum.auto()
    DECLARING_QUEUES = enum.auto()
    DELETING_QUEUES = enum.auto()
    PUBLISHING = enum.auto()
    RECONNECTING = enum.auto()
    SETTING_QOS = enum.auto()
    STOPPED = enum.auto()
    STOPPING = enum.auto()
    READY = enum.auto()


class AioPika:
    def __init__(self):
        self._state = None

    def _log_state_error(
        self, cant_message: str, cant_args: Iterable = None, *states: AsyncState
    ):
        LOGGER.error(
            ("%s CAN'T " + cant_message + ". EXPECTED STATE(S): %s. CURRENT STATE: %s"),
            self,
            *(cant_args if isinstance(cant_args, Iterable) else []),
            states,
            self.state,
        )

    @property
    def is_ready(self) -> bool:
        """
        Note: Ready refers to ready for use, and assumes minimal configuration is complete.
        """
        return self._state is AsyncState.READY

    @property
    def is_stopping(self) -> bool:
        return self._state is AsyncState.STOPPING

    @property
    def is_stopped(self) -> bool:
        """
        Note: Stopped refers to the graceful closure of all relevant amqp connections.
        """
        return self._state is AsyncState.STOPPED

    @property
    def state(self) -> AsyncState:
        return self._state

    async def _wait_until(
        self,
        wait_fn: Callable[[], bool],
        error_fn: Callable[[], None],
        timeout: int = None,
    ):
        timer = 0
        wait_result = wait_fn()
        while not wait_result and (not timeout or (timeout and timer < timeout)):
            wait_result = wait_fn()
            timer += 1
            await asyncio.sleep(1)

        if timeout and timer >= timeout:
            error_fn()

    async def _wait_until_state(self, *states: AsyncState, timeout: int = None):
        def wait_fn() -> bool:
            return self._state in states

        def error_fn():
            LOGGER.error(
                "%s STOPPED WAITING FOR %s in %s. Timeout of %i exceeded",
                self,
                self._state,
                states,
                timeout,
            )

        return await self._wait_until(wait_fn, error_fn, timeout=timeout)

    async def _wait_until_property(self, property_name: str, timeout: int = None):
        def wait_fn() -> bool:
            return getattr(self, property_name)

        def error_fn():
            LOGGER.error(
                "%s STOPPED WAITING FOR %s. Timeout of %i exceeded",
                self,
                property_name,
                timeout,
            )

        return await self._wait_until(wait_fn, error_fn, timeout=timeout)

    async def wait_until_ready(self, timeout: int = None):
        """
        Wait until either ready condition (as defined by is_ready property), timeout, or forever

        If timeout reached, logs an error message.
        """
        await self._wait_until_property("is_ready", timeout=timeout)

    def set_state(self, state: AsyncState):
        LOGGER.debug("Setting state of %s to %s", self, state)
        self._state = state


class AioConnectionPool(AioPika):
    """
    Wraps aio-pika's ConnectionPool.

    Provides convenience methods for acquiring/handling aio-pika Connection instances.
    """

    def __init__(self, max_connections: int):
        super().__init__()
        self.pool = None
        asyncio.create_task(self.setup_pool(max_connections=max_connections))

    async def acquire(
        self,
    ) -> Optional[PoolItemContextManager[T]]:  # T is aio pika's TypeVar("T")
        """
        Acquires aio-pika Connection instance.

        Note: Must be used in an async with.
        """
        if self.is_ready:
            try:
                return self.pool.acquire()
            except:
                LOGGER.exception("POOL %s could not aquire channel", self)
        else:
            self._log_state_error("could not acquire channel", None, AsyncState.READY)
        return

    async def close(self):
        """
        Perform graceful shutdown of Connection instance.
        """
        LOGGER.info("Closing Pool: %s", self)
        self.set_state(AsyncState.STOPPING)
        try:
            await self.pool.close()
            LOGGER.debug("Closed Pool: %s", self)
            self.set_state(AsyncState.STOPPED)
        except:
            LOGGER.exception("COULD NOT CLOSE POOL: %s", self)

    @staticmethod
    async def get_connection() -> Connection:
        """
        Acquire asynchronous Connection using aio-pika.
        """
        return await get_aio_async_connection()

    async def setup_pool(self, max_connections: int):
        """
        Configure and acquire aio-pika Pool.
        """
        LOGGER.debug("SETTING UP POOL for %s", self)
        self.pool = Pool(
            self.get_connection,
            max_size=max_connections,
            loop=asyncio.get_event_loop(),
        )
        LOGGER.debug("SET UP POOL FOR %s", self)
        self.set_state(AsyncState.READY)


class AioClient(AioPika):
    def __init__(
        self,
        exchange: str = None,
        client_name: str = None,
        pool: AioConnectionPool = None,
    ):
        super().__init__()
        self.pool = pool
        self.client_name = client_name
        self._connection = None
        self._channel = None
        self._exchange_name = exchange
        self._exchange = None
        asyncio.create_task(self.connect())

    def __repr__(self):
        return self.client_name or super().__repr__()

    @property
    def connected_states(self) -> Tuple[AsyncState]:
        return (AsyncState.CONNECTED, AsyncState.READY, AsyncState.CONSUMING)

    @property
    def exchange_name(self) -> str:
        return self._exchange_name or "default exchange" if self._exchange else None

    @property
    def is_connected(self) -> bool:
        return self._state in self.connected_states or (
            self._connection
            and self._channel
            and not (self.is_stopping or self.is_stopped)
        )

    @property
    def is_consuming(self) -> bool:
        return self._state is AsyncState.CONSUMING

    @property
    def is_ready(self) -> bool:
        return bool(super().is_ready or self.is_connected and self._exchange)

    @property
    def is_stopped_or_stopping(self) -> bool:
        return self.state in (AsyncState.STOPPED, AsyncState.STOPPING)

    async def wait_until_connected(self, timeout: int = None):
        await self._wait_until_state(
            *self.connected_states,
            timeout=timeout,
        )

    async def bind_queue(
        self,
        queue_name: str,
        exchange_name: str = None,
        routing_key: str = "",
        timeout: int = None,
    ):
        base_message = "queue: %s w/ key: %s to exchange: %s"
        use_exchange_name = exchange_name or self._exchange_name
        base_args = (queue_name, routing_key, use_exchange_name)

        if self.is_connected:
            if use_exchange_name != self._channel.default_exchange.name:
                LOGGER.debug("%s binding " + base_message, self, *base_args)
                exchange = (
                    await self.get_exchange(use_exchange_name)
                    if exchange_name
                    else self._exchange
                )
                queue = await self.get_queue(queue_name)
                if queue:
                    try:
                        return await queue.bind(exchange, routing_key, timeout=timeout)
                    except:
                        LOGGER.exception(
                            "%s COULD NOT BIND " + base_message,
                            self,
                            *base_args,
                        )
                        return
                else:
                    LOGGER.debug("NO QUEUE: %s", queue_name)

            LOGGER.error(
                "%s COULD NOT BIND "
                + base_message
                + ". NO EXCHANGE_NAME OR NON-DEFAULT CURRENT EXCHANGE",
                self,
                *base_args,
            )
        else:
            self._log_state_error(
                "bind" + base_message, base_args, *self.connected_states
            )
        return

    async def close(self):
        if not self.pool:
            try:
                LOGGER.debug("STOPPING: %s", self)
                self.set_state(AsyncState.STOPPING)
                await self._channel.close()
                await self._connection.close()
                self._channel = None
                self._connection = None
                self.set_state(AsyncState.STOPPED)
            except:
                LOGGER.exception("COULD NOT CLOSE %s", self)
                raise
        else:
            LOGGER.error("COULD NOT CLOSE. %s is connected to a pool!", self)

    async def connect(self):
        """
        Instantiate and set aio-pika Connection.

        Prefers AioConnectionPool if offered.

        Note: If an exchange name is provided to __init__, exchange must already exist.
          Otherwise, connect will fail with an error log.
        """
        LOGGER.info("Connecting %s", self)
        self.set_state(AsyncState.CONNECTING)
        if self.pool:
            try:
                LOGGER.debug("POOL: %s", self.pool.pool)
                async with self.pool.pool.acquire() as connection:
                    self._connection = connection
            except:
                LOGGER.exception(
                    "%s COULD NOT AQUIRE CONNECTION FROM POOL: %s", self, self.pool
                )
        else:
            try:
                self._connection = await get_aio_async_connection()
            except:
                LOGGER.exception("%s COULD NOT CONNECT", self)

        try:
            LOGGER.debug(
                "Declaring channel for %s. Connection: %s", self, self._connection
            )
            try:
                self._channel = await self._connection.channel()
            except:
                LOGGER.exception("%s COULD NOT DECLARE CHANNEL", self)
            self.set_state(AsyncState.CONNECTED)
            if self._exchange_name is not None:
                if self._exchange_name != "":
                    self._exchange = await self.get_exchange(self._exchange_name)
                else:
                    LOGGER.debug("Using default exchange for %s", self)
                    self._exchange = self._channel.default_exchange
                    self.set_state(AsyncState.READY)
        except:
            LOGGER.exception("COULD NOT DECLARE CHANNEL FOR %s", self)

    async def declare_exchange(
        self,
        name: str,
        exchange_type: str = "direct",
        durable: bool = False,
        auto_delete: bool = False,
        timeout: int = None,
        **kwargs: Any,
    ) -> Optional[Exchange]:
        if self.is_connected:
            LOGGER.info(
                "Declaring exchange: %s of type: %s, using: %s",
                name,
                exchange_type,
                self,
            )
            self.set_state(AsyncState.DECLARING_EXCHANGE)
            ret = await self._channel.declare_exchange(
                name,
                type=ExchangeType(exchange_type),
                durable=durable,
                auto_delete=auto_delete,
                timeout=timeout,
                **kwargs,
            )
            if self.state is AsyncState.DECLARING_EXCHANGE:
                self.set_state(
                    AsyncState.READY if self.is_ready else AsyncState.CONNECTED
                )
            return ret

        self._log_state_error("declare exchange: %s", (name,), *self.connected_states)
        return

    async def declare_queue(
        self,
        name: str = None,
        durable: bool = False,
        exclusive: bool = False,
        auto_delete: bool = False,
        timeout: int = None,
        **kwargs: Any,
    ) -> Optional[Queue]:
        ret = None
        if self.is_connected:
            LOGGER.debug("Declaring queue: %s, using: %s", name or "*RANDOM*", self)
            self.set_state(AsyncState.DECLARING_QUEUES)
            ret = await self._channel.declare_queue(
                name,
                durable=durable,
                exclusive=exclusive,
                auto_delete=auto_delete,
                timeout=timeout,
                **kwargs,
            )
            self.set_state(AsyncState.READY if self.is_ready else AsyncState.CONNECTED)
            return ret

        self._log_state_error("declare queue: %s", (name,), *self.connected_states)
        return ret

    async def delete_exchange(
        self,
        exchange_name: str = None,
        if_unused: bool = True,
        timeout: int = None,
    ):
        if self.is_connected:
            use_exchange_name = exchange_name or self._exchange_name
            if use_exchange_name != self._channel.default_exchange.name:
                exchange = await self.get_exchange(use_exchange_name)
                LOGGER.info("%s deleting exchange: %s", self, use_exchange_name)
                try:
                    return await exchange.delete(if_unused=if_unused, timeout=timeout)
                except:
                    LOGGER.exception(
                        "%s CANNOT DELETE EXCHANGE: %s", self, use_exchange_name
                    )
            else:
                LOGGER.error("%s CANNOT DELETE EXCHANGE: %s.", self, use_exchange_name)

        self._log_state_error(
            "delete exchange: %s",
            (exchange_name or self._exchange_name,),
            *self.connected_states,
        )

    async def delete_queue(
        self,
        queue_name: str,
        if_unused: bool = True,
        if_empty: bool = True,
        timeout: int = None,
    ):
        if self.is_connected:
            try:
                queue = await self.get_queue(queue_name)
                if queue:
                    LOGGER.debug("%s deleting queue: %s", self, queue_name)
                    return await queue.delete(
                        if_unused=if_unused, if_empty=if_empty, timeout=timeout
                    )
            except:
                LOGGER.exception("%s FAILED TO DELETE QUEUE: %s", self, queue_name)
        else:
            self._log_state_error(
                "delete queue: %s", (queue_name,), *self.connected_states
            )
        return

    async def get_exchange(self, name: str) -> Optional[Exchange]:
        if self.is_connected:
            try:
                LOGGER.debug("%s GETTING EXCHANGE: %s.", self, name)
                exchange = await self._channel.get_exchange(name)
                return exchange
            except:
                LOGGER.exception("%s COULD NOT GET EXCHANGE: %s.", self, name)
        else:
            self._log_state_error("get exchange: %s", (name,), *self.connected_states)

    async def get_queue(self, name: str) -> Optional[Queue]:
        if self.is_connected:
            try:
                LOGGER.debug("%s is getting queue: %s", self, name)
                queue = await self._channel.get_queue(name)
                if queue:
                    return queue
                LOGGER.error("%s COULD NOT FIND QUEUE: %s", self, name)
            except:
                LOGGER.exception("%s could not get queue: %s", self, name)
        else:
            self._log_state_error("get queue: %s", (name,), AsyncState.READY)

    async def publish_message(
        self,
        routing_key: str,
        message: str,
        headers: MutableMapping[
            str,
            Union[FieldValue, Iterable[FieldValue]],
        ] = None,
        content_type: str = None,
        delivery_mode: str = "persistent",
        correlation_id: str = None,
        reply_to: str = None,
        expiration: Union[dt.datetime, int, float, dt.timedelta] = None,
        message_id: str = None,
        **properties: Any,
    ) -> Optional[Union[Basic.Ack, Basic.Nack, Basic.Reject]]:
        """
        Publish a message to RabbitMQ.

        Only publishes if class is connected.
        """
        ret = None
        if self.is_ready:
            previous_state = self._state
            try:
                if isinstance(message, dict):
                    message = json.dumps(message)
                if isinstance(message, str):
                    message = message.encode("utf-8")
                elif not isinstance(message, bytes):
                    LOGGER.error(
                        "%s COULD NOT PUBLISH TO exchange: %s with key: %s. Unknown message: %s, of type: %s",
                        self,
                        self.exchange_name,
                        routing_key,
                        message,
                        type(message),
                    )
                    return

                delivery_mode = getattr(DeliveryMode, delivery_mode.upper())
                self.set_state(AsyncState.PUBLISHING)
                ret = await self._exchange.publish(
                    aio_pika.Message(
                        message,
                        headers=headers,
                        content_type=content_type,
                        delivery_mode=delivery_mode,
                        correlation_id=correlation_id,
                        reply_to=reply_to,
                        expiration=expiration,
                        message_id=message_id,
                        **properties,
                    ),
                    routing_key=routing_key,
                )
                LOGGER.info(
                    "%s published message: %s, to %s with key: %s",
                    self,
                    message,
                    self.exchange_name,
                    routing_key,
                )
            except:
                LOGGER.exception("ERROR: COULD NOT PUBLISH MESSAGE")
            finally:
                self.set_state(previous_state)
        else:
            LOGGER.error(
                "%s COULD NOT PUBLISH TO exchange: %s with key: %s. Channel closed. Current state: %s",
                self,
                self.exchange_name,
                routing_key,
                self._state,
            )
        return ret

    async def set_exchange(self, name: str):
        """
        Aquire exchange (by name) and bind to current inst (as default for future actions)
        """
        if self.is_connected:
            self.set_state(AsyncState.CONNECTING)
            exchange = await self.get_exchange(name)
            self._exchange = exchange
            self._exchange_name = name
            if self.is_ready:
                self.set_state(AsyncState.READY)
        else:
            self._log_state_error("set exchange: %s", (name,), *self.connected_states)

    async def set_prefetch_count(
        self, n: str, timeout: bool = None
    ) -> Optional[Basic.QosOk]:
        """
        Limit the number of unacknowledged messages on a connection during consumption.
        """
        if self.is_connected or self.is_consuming:
            previous_state = self._state
            self.set_state(AsyncState.SETTING_QOS)
            ret = await self._channel.set_qos(prefetch_count=n, timeout=timeout)
            self.set_state(
                AsyncState.CONSUMING
                if previous_state == AsyncState.CONSUMING
                else AsyncState.READY
                if self.is_ready
                else AsyncState.CONNECTED
            )
            return ret
        else:
            self._log_state_error(
                "set %s's prefetch_count to %s.", (self, n), *self.connected_states
            )
        return

    def set_state(self, state: AsyncState):
        LOGGER.debug("Setting state of %s to %s", self, state)
        self._state = state

    async def start_consuming(
        self,
        queue_name: str,
        callback: Callable[
            [AbstractIncomingMessage],
            None,
        ],
        no_ack: bool = False,
        exclusive: bool = False,
        timeout: int = None,
        **kwargs: Any,
    ) -> Optional[ConsumerTag]:
        """
        Start listening on the given queue.

        Note, only consumes if in ready state. Otherwise, logs error message.
        """
        LOGGER.debug(
            "%s is issuing consumer related RPC commands on queue: %s", self, queue_name
        )
        if self.is_ready:
            try:
                queue = await self.get_queue(queue_name)
                if queue:
                    ret = await queue.consume(
                        callback=callback,
                        no_ack=no_ack,
                        exclusive=exclusive,
                        consumer_tag=kwargs.get("consumer_tag"),
                        timeout=timeout,
                        arguments=kwargs,
                    )
                    LOGGER.info("%s is now consuming: %s", self, queue_name)
                    self.set_state(AsyncState.CONSUMING)
                    return ret
            except:
                LOGGER.exception(
                    "%s FAILED TO CONSUME FROM %s", self, self.exchange_name
                )
        else:
            self._log_state_error(
                "consume from %s", (self.exchange_name,), AsyncState.READY
            )
        return

    async def wait_until_connected(self, timeout: int = None):
        await self._wait_until_property(
            "is_connected",
            timeout=timeout,
        )

    async def wait_until_consuming(self, timeout: int = None):
        await self._wait_until_state(AsyncState.CONSUMING, timeout=timeout)
