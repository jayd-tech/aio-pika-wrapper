# aio-pika-wrapper
The aio-pika-wrapper is a convenience tool for rapid deployment of aio pika's (https://github.com/mosquito/aio-pika)
asynchronous AMQP library. aio-pika-wrapper provides an object oriented client to centralize operations per connection, pool (to centralize operations per connection group), and internal states (per client) for easier understanding of what each connection is
doing (e.g, publishing/receiving a message on queue {X} of exchange {Y} of type {Z}).

## Installation
To install, point your package manager's git command to the following address: https://gitlab.com/jayd-tech/aio-pika-wrapper.git


## Getting started
If you're already familiar with rabbitmq and asyncio, basic usage examples are provided below.

### Create connection
```
from aio_pika_wrapper.client import AioClient

client = AioClient(exchange="", client_name="TestClient")
await client.wait_until_ready(timeout=5)
# do something with client #
await client.close()
```

```
from aio_pika_wrapper.client import AioClientPool

pool = AioConnectionPool(max_connections=5)
await pool.wait_until_ready(5)
for _ in range(5):
  client = AioClient(exchange="", pool=pool)
  await client.wait_until_ready(timeout=CLIENT_ACTION_TIMEOUT)
  asyncio.create_task(# do something with client #)
await # confirm all clients are finished #
await pool.close()
```

### Declare/Delete exchange
```
# Default
producer_default = AioClient(exchange="", client_name=client_name)
producer_default
```

```
# New exchange
publisher = AioClient(client_name="Producer")
await publisher.wait_until_connected(timeout=5)
await publisher.declare_exchange(
    name="my_exchange", exchange_type="topic", timeout=5
)
await publisher.set_exchange("my_exchange")
await publisher.delete_exchange("my_exchange", if_unused=True)
```

### Declare/Delete queue
```
publisher = AioClient(exchange="my_exchange", client_name="Producer")  # pre-existing exchange
random_queue = await publisher.declare_queue(name=None)  # use random name
await publisher.bind_queue(random_queue.name)
await publisher.delete_queue(random_queue.name, if_unused=False, if_empty=False)
```

### Publish message
```
publisher = AioClient("topic_exchange", client_name="Producer")  # pre-existing exchange
await publisher.wait_until_ready(timeout=5)
await publisher.publish_message(
    "sweet.topic.of.mine",
    message,
    content_type="text/plain",
    delivery_mode="not_persistent",
)
```

### Subscribe to queue
```
async def callback(message):
    async with message.process(requeue=True, reject_on_redelivered=True):
        if message.content_type == "text/plain":
            do_something()
        else:
            LOGGER.error("TYPE ERROR WHILE PROCESSING: %s", message.body)
            raise

consumer = AioClient("topic_exchange", client_name="Consumer")  # pre-existing exchange
await consumer.wait_until_connected(timeout=5)
random_queue = await consumer.get_queue(queue_name)
await consumer.start_consuming(random_queue.name, callback, timeout=5)
```

## Additonal functionality
See tests for more advanced functionality (i.e., asyncio rpc implementation).

If you're new to rabbitmq, a great place to start is the rabbitmq official tutorial (https://www.rabbitmq.com/tutorials/tutorial-one-python.html)!

For more detailed information, all functions are clearly documented with docstrings. If you find any confusing, feel free to open an issue!


## Running tests
This repo is meant for testing in docker. In addition to the docker-compose and scripts provided in config/docker, Python-Docker (https://github.com/jusjayson/Python-Docker) makes it easy to build and launch this repo and test using ``pytest app/tests``.

After downloading Python-Docker, in this repository, prepare the relevant env files (see make vars: DOCKER_COMMON_ENV_PATH and DOCKER_SPECIFIC_ENV_PATH, and all DOCKER_* variables). Then run
``make build-base-image; make build-project; make launch-local-project;``



## Contributing
I am happy to listen if you have any ideas on improving the project and are willing to contribute! See the section on running tests in order to get the local docker environment running. All needed environmental variables are present in the project's Makefile and docker-compose file.


## License
MIT

## Project status
Active