import asyncio
import io
import threading

import pytest
from tenacity import retry, stop_after_attempt, wait_fixed

from aio_pika_wrapper.client import AioClient, AioConnectionPool, AsyncState


CLIENT_ACTION_TIMEOUT = 5


@pytest.mark.asyncio
async def test_aioclient_ready():
    client = AioClient(exchange="", client_name="Starter")
    try:
        await client.wait_until_ready(5)
        assert client.state == AsyncState.READY
    finally:
        await client.close()


@pytest.mark.asyncio
async def test_connection_pool():
    """
    Confirm client can connect using pool
    """
    pool = AioConnectionPool(max_connections=1)
    await pool.wait_until_ready(5)
    client = AioClient(exchange="", pool=pool)
    await client.wait_until_ready(timeout=CLIENT_ACTION_TIMEOUT)
    await pool.close()


@pytest.mark.asyncio
async def test_delete_queue():
    in_memory_files = {}
    access_files_lock = threading.Lock()

    @retry(reraise=True, stop=stop_after_attempt(3), wait=wait_fixed(5))
    async def assert_w_retry():
        with access_files_lock:
            assert "First" not in (content := in_memory_files["test.txt"].getvalue())
            assert "Second" in content

    async def consume(pool):
        consumer = AioClient(exchange="", client_name="Consumer", pool=pool)

        async def callback(message):
            async with message.process():
                in_memory_files["test.txt"] = (f := io.StringIO())
                f.write(message.body.decode("utf-8"))

        await consumer.wait_until_ready(timeout=CLIENT_ACTION_TIMEOUT)
        await consumer.declare_queue(
            "test", durable=True, timeout=CLIENT_ACTION_TIMEOUT
        )
        await consumer.start_consuming("test", callback, timeout=CLIENT_ACTION_TIMEOUT)

    async def produce(message, client_name, pool):
        producer = AioClient(exchange="", client_name=client_name, pool=pool)

        await producer.wait_until_ready(timeout=CLIENT_ACTION_TIMEOUT)
        await producer.declare_queue(
            "test", durable=True, timeout=CLIENT_ACTION_TIMEOUT
        )
        await producer.publish_message("test", message, delivery_mode="not_persistent")

    pool = AioConnectionPool(4)
    await pool.wait_until_ready(timeout=CLIENT_ACTION_TIMEOUT)
    starter = AioClient(exchange="", client_name="Starter", pool=pool)
    await starter.wait_until_connected(timeout=CLIENT_ACTION_TIMEOUT)
    await produce("First", "Initial Producer", pool)
    await starter.delete_queue(
        "test", if_unused=False, if_empty=False, timeout=CLIENT_ACTION_TIMEOUT
    )
    asyncio.create_task(consume(pool))
    await produce("Second", "Second Producer", pool)

    try:
        await assert_w_retry()
    finally:
        await starter.delete_queue("test", if_unused=False, if_empty=False)
        await pool.close()
