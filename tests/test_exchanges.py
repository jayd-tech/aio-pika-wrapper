import asyncio
import io
import logging
import threading

import pytest
from tenacity import retry, stop_after_attempt, wait_fixed

from aio_pika_wrapper.client import AioClient, AioConnectionPool, AsyncState

CLIENT_ACTION_TIMEOUT = 5


@pytest.mark.asyncio
async def test_direct_exchange():
    in_memory_files = {"test.txt": io.StringIO()}
    access_files_lock = threading.Lock()
    message = "Hi There"

    @retry(reraise=True, stop=stop_after_attempt(3), wait=wait_fixed(3))
    async def assert_w_retry():
        with access_files_lock:
            assert in_memory_files["test.txt"].getvalue() == message

    async def consume(consumer):
        async def callback(message):
            async with message.process():
                with access_files_lock:
                    in_memory_files["test.txt"].write(message.body.decode("utf-8"))

        await consumer.wait_until_ready(timeout=5)
        await consumer.declare_queue(name="my_name_doesnt_matter")
        await consumer.start_consuming("my_name_doesnt_matter", callback)

    publisher = AioClient(client_name="Producer")

    await publisher.wait_until_connected(timeout=5)
    try:
        await publisher.declare_exchange(
            name="my_exchange", exchange_type="direct", timeout=5
        )
        await publisher.set_exchange("my_exchange")
        await publisher.declare_queue(name="my_name_doesnt_matter")
        await publisher.bind_queue("my_name_doesnt_matter", routing_key="hello")
        assert publisher._exchange and publisher._exchange_name == "my_exchange"
        await publisher.publish_message(
            "hello", message, delivery_mode="not_persistent"
        )
        consumer = AioClient(exchange="my_exchange", client_name="Consumer")

        asyncio.create_task(consume(consumer))
        await assert_w_retry()
    finally:
        await publisher.delete_queue(
            "my_name_doesnt_matter", if_unused=False, if_empty=False
        )
        await publisher.delete_exchange("my_exchange", if_unused=False)
        await publisher.close()
        await consumer.close()


@pytest.mark.asyncio
async def test_topic_exchange():
    in_memory_files = {"test.txt": io.StringIO()}
    access_files_lock = threading.Lock()
    message = "A Corny Dad Cries"

    @retry(reraise=True, stop=stop_after_attempt(3), wait=wait_fixed(3))
    async def assert_w_retry():
        with access_files_lock:
            assert in_memory_files["test.txt"].getvalue() == message

    async def consumer(pool, queue_name):
        async def callback(message):
            async with message.process(requeue=True, reject_on_redelivered=True):
                if message.content_type == "text/plain":
                    with access_files_lock:
                        in_memory_files["test.txt"].write(message.body.decode("utf-8"))
                else:
                    logging.error("TYPE ERROR WHILE PROCESSING: %s", message.body)
                    raise

        consumer = AioClient("topic_exchange", client_name="Consumer", pool=pool)
        await consumer.wait_until_connected(timeout=5)
        random_queue = await consumer.get_queue(queue_name)
        await consumer.start_consuming(random_queue.name, callback, timeout=5)

    async def publish(pool):
        publisher = AioClient("topic_exchange", client_name="Producer", pool=pool)
        await publisher.wait_until_ready(timeout=5)
        await publisher.publish_message(
            "sweet.topic.of.mine",
            message,
            content_type="text/plain",
            delivery_mode="not_persistent",
        )

    pool = AioConnectionPool(max_connections=3)
    try:
        starter = AioClient(client_name="Starter", pool=pool)
        await starter.wait_until_connected(timeout=5)
        await starter.declare_exchange(
            "topic_exchange", exchange_type="topic", timeout=5
        )
        random_queue = await starter.declare_queue(timeout=5)
        await starter.bind_queue(
            random_queue.name, "topic_exchange", routing_key="sweet.*.of.*"
        )
        asyncio.create_task(consumer(pool, random_queue.name))
        await publish(pool)
        await assert_w_retry()
    finally:
        await starter.delete_queue(random_queue.name, if_unused=False, if_empty=False)
        await starter.delete_exchange("topic_exchange", if_unused=False, timeout=5)
        await pool.close()
